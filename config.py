import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = os.environ.get('SECRET_KEY',
                                '51f52814-0071-11e6-a247-000ec6c2372c')
    JWT_SECRET_KEY = os.environ.get('JWT_SECRET_KEY', SECRET_KEY)
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL', '')
    SQLALCHEMY_POOL_RECYCLE = 60
    CELERY_CONFIG = {}
    LB = os.environ.get('LB', '')
    URL_PREFIX = '/api/feeds'
    TOKEN_EXPIRE_MINUTES = 1440
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    TEMP_FOLDER = 'temp'
    PARSE_DELAY = 60


class DevelopmentConfig(Config):
    DEBUG = True
    SESSION_COOKIE_SECURE = False
    TEMPLATES_AUTO_RELOAD = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///' + basedir + os.sep + 'feeds.db'
    TEMP_FOLDER = 'temp'
    PARSE_DELAY = 10


class ProductionConfig(Config):
    pass


class TestingConfig(ProductionConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test.db'
    UPLOAD_FOLDER = './tmp'


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'testing': TestingConfig
}
