import os
from flask import Blueprint, render_template, session, redirect, url_for, request, current_app as app, g
from listenup_common.auth import token_optional_auth
from listenup_common.api_response import ApiException, ApiMessage, ApiResult
from feeds.tasks import test_task, create_feed
from .models import Feed, CachedPodcastInfo
from . import TOP_X_GUID_PREFIX, DEFAULT_FEED_GUID, HOT_FEED_GUID

main = Blueprint('main', __name__, url_prefix='/api/feeds')

root_path = os.path.dirname(os.path.abspath(__file__))


@main.route("/test")
def test():
    test_task.apply_async(args=[], countdown=15)
    return ApiResult(value={"hi": "this is a test"}, status=200).to_response()


@main.route("/")
@token_optional_auth.login_required
def get_my_feed():
    if not g.current_user_guid:  # not logged in
        feed = Feed.query.filter_by(guid=DEFAULT_FEED_GUID).one_or_none()
    else:
        feed = Feed.query.filter_by(guid=g.current_user_guid).one_or_none()
        if not feed:
            create_feed.apply_async(args=[g.current_user_guid])
            feed = Feed.query.filter_by(guid=DEFAULT_FEED_GUID).one_or_none()

    if not feed:
        raise ApiException(message=f"Failed to find feed for user: {g.current_user_guid}", status=404)
    return ApiResult(value=feed.to_dict(), status=200).to_response()


@main.route("/hot")
def get_hot_feed():
    feed = Feed.query.filter_by(guid=HOT_FEED_GUID).one_or_none()
    if not feed:
        raise ApiException(message=f"Failed to find feed for hot podcasts", status=404)
    return ApiResult(value=feed.to_dict(), status=200).to_response()


@main.route("/top/<int:count>")
def get_top_x_feed(count):
    feed = Feed.query.filter_by(guid=TOP_X_GUID_PREFIX + str(count)).one_or_none()
    if not feed:
        raise ApiException(message=f"Failed to find feed for top: {count}", status=404)
    return ApiResult(value=feed.to_dict(), status=200).to_response()


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
