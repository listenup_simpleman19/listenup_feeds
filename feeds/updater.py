from listenup_common.logging import get_logger
from listenup_common.wrappers import listens
from flask import current_app as app
from .models import Feed, CachedPodcastInfo
from . import TOP_X_GUID_PREFIX, HOT_FEED_GUID, DEFAULT_FEED_GUID, db
import time

logger = get_logger(__name__)


def create_or_update_personal_feed(feed_guid: str):
    logger.warn(f"Updating personal feed for: {feed_guid} not yet implemented")


# TODO this isn't right but good enough for now
def update_hot_feed():
    feed_guid = HOT_FEED_GUID
    count = 10
    podcasts = listens.get_top_listen_count(count)
    if not podcasts:
        logger.error(f"Failed to get top {count} podcasts from listens service")
        return
    elif len(podcasts) != count:
        logger.warn(f"Updating {feed_guid} and got less than {count} podcasts")
    _create_or_update_feed(feed_guid, podcasts, "Hotest Podcasts on ListenUp")


# TODO this isn't right but good enough for now
def update_default_feed():
    feed_guid = DEFAULT_FEED_GUID
    count = 10
    podcasts = listens.get_top_listen_count(count)
    if not podcasts:
        logger.error(f"Failed to get top {count} podcasts from listens service")
        return
    elif len(podcasts) != count:
        logger.warn(f"Updating {feed_guid} and got less than {count} podcasts")
    _create_or_update_feed(feed_guid, podcasts, "Hotest Podcasts on ListenUp")


def update_top_x_feed(count):
    feed_guid = TOP_X_GUID_PREFIX + str(count)
    podcasts = listens.get_top_listen_count(count)
    if not podcasts:
        logger.error(f"Failed to get top {count} podcasts from listens service")
        return
    elif len(podcasts) != count:
        logger.warn(f"Updating {feed_guid} and got less than {count} podcasts")
    _create_or_update_feed(feed_guid, podcasts, f"Top {count} Podcasts on ListenUp")


def _create_or_update_feed(feed_guid: str, podcasts, description=""):
    feed = Feed.query.filter_by(guid=feed_guid).one_or_none()
    if not feed:
        logger.info(f"Feed {feed_guid} did not exist so creating it")
        feed = Feed(force_guid=feed_guid)
        feed.description = description
        db.session.add(feed)
    feed.podcasts.clear()
    for podcast in podcasts:
        pod_guid = podcast.get("podcast_guid", None)
        if not pod_guid:
            logger.error(f"Could not find guid key in {podcast}")
        else:
            db_podcast = CachedPodcastInfo.query.filter_by(podcast_guid=pod_guid).one_or_none()
            if not db_podcast:
                db_podcast = CachedPodcastInfo(podcast_guid=pod_guid)
            db.session.add(db_podcast)
            feed.podcasts.append(db_podcast)
    db.session.commit()


def update_global_feeds():
    logger.info("Starting global feed updater")
    while True:
        try:
            update_hot_feed()
            update_default_feed()
            update_top_x_feed(100)
            update_top_x_feed(50)
            update_top_x_feed(10)
            update_top_x_feed(5)
            update_top_x_feed(1)
        except Exception as ex:
            logger.exception("Fatal error while trying to updates global feeds", exc_info=True)
        logger.debug("Sleeping for: " + str(app.config.get("PARSE_DELAY", 60)))
        time.sleep(app.config.get("PARSE_DELAY", 60))
