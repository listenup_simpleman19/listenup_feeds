from feeds.updater import create_or_update_personal_feed
from feeds import celery
import os

RETRY_TIMES = 5

config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')


@celery.task
def test_task():
    print("celery task")


@celery.task
def create_feed(feed_guid):
    from feeds import create_app
    app = create_app(config_name)
    with app.app_context():
        create_or_update_personal_feed(feed_guid)
