from datetime import datetime
from flask_sqlalchemy import SQLAlchemy
from listenup_common.utils import guid
from listenup_common.wrappers.podcast import get_podcasts_info_by_guids
import enum

db = SQLAlchemy()


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)


feed_to_podcast_table = db.Table('feed_to_podcast',
                                 db.Column('feed_id', db.Integer, db.ForeignKey('feed.id')),
                                 db.Column('podcast_id', db.Integer, db.ForeignKey('cached_podcast.id')))


class Feed(BaseModel):
    __tablename__ = "feed"
    guid = db.Column(db.String(32), nullable=False, unique=True)
    podcasts = db.relationship("CachedPodcastInfo", secondary=feed_to_podcast_table)
    description = db.Column(db.String(1000), nullable=False, default="")

    def __init__(self, force_guid: str = None):
        if not force_guid:
            self.guid = guid()
        else:
            self.guid = force_guid

    def to_dict(self):
        d = {
            'guid': self.guid,
            'creation_timestamp': self.creation_timestamp,
        }
        podcast_guids = [p.podcast_guid for p in self.podcasts]
        d['podcasts'] = get_podcasts_info_by_guids(guids=podcast_guids)
        return d


class CachedPodcastInfo(BaseModel):
    __tablename__ = "cached_podcast"
    podcast_guid = db.Column(db.String(32), nullable=False, unique=True)

    def to_dict(self):
        return {
            'podcast_guid': self.podcast_guid
        }
